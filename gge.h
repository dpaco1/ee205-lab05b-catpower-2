/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 17_Feb_2022
////////////////////////////////////
#pragma once
const double GGE_IN_A_JOULE = 1/1.213e8 ;
const char GGE = 'g';
extern double fromGgeToJoule( double gge ) ;
extern double fromJouleToGge( double joule ) ;
